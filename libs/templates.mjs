import { len, range, join, zipWith } from './functions.mjs'

export const trimLines = str =>
  str
    .split( '\n' )
    .map( s => s.trim(  ) )
    .join( '' )

// Trims all whitespace between lines and replaces nullish values with strings
export const html = ( strings, ...vars ) =>
  join
   ( zipWith
      ( ( a, b ) =>
          trimLines( a ) + ( b || '' ) )
      ( strings )
      ( vars ) )
