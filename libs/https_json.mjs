import https from 'https'

export const https_json = url =>
  new Promise( ( resolve, reject ) => {
    const request = https.get
      ( url
      , response => {
          if ( response.statusCode >= 400 )
            reject( response )
          
          let data = [  ]

          response.on( 'data', d => data.push( d ) )

          response.on( 'end', (  ) => {
            try {
              const data_string = Buffer
                .concat( data )
                .toString(  )

              const json_data = JSON.parse( data_string )

              resolve( json_data )
            } catch ( e ) {
              reject( e )
            }
          } )
        }
      )

    request.on( 'error', reject )

    request.end(  )
  } )
