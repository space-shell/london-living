export const ansi_reset = '\u001b[0m'

export const ansi_red = '\u001b[31m'

export const ansi_green = '\u001b[32m'

export const red = message =>
  ansi_red + message + ansi_reset

export const green = message =>
  ansi_green + message + ansi_reset
