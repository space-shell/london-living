import { sleep } from './io.mjs'
import { tap } from './functions.mjs'
import { run, test, assert } from './test.mjs'

import
  { pipe
  , pipeAsync
  }
from './functions.mjs'


run
  ( 'Functions' )
  ( test
      ( 'Async pipe function math'
      , async (  ) => {
          const res = await pipeAsync
            ( async x =>
                {
                  await sleep( 10 )

                  return x + 3
                }
            , async x => x ** 3
            ) ( 4 )

          assert( res === 343  )
        }
      )
  , test
      ( 'Pipe function math'
      , async (  ) => {
          const res = pipe
            ( x => x + 3
            , x => x ** 3
            ) ( 4 )

          assert( res === 343  )
        }
      )
  )
