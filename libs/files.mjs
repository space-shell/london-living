import fs from 'fs'
import path from 'path'

export const getFiles = async ( dir ) => {

  const dirents = await fs.promises.readdir( dir, { withFileTypes: true } )

  const files = await Promise.all( dirents.map( ( dirent ) => {
  
    const res = path.resolve(dir, dirent.name)

    return dirent.isDirectory(  ) ? getFiles( res ) : res
  } ) );

  return files.flat( Infinity );
}

export const filePathDirPath = path =>
  path
    .pathname
    .split( '/' )
    .slice( 0, -1 )
    .join( '/' )
