import { red, green } from './ansi.mjs'
import { len } from './functions.mjs'

const Fail = new Error( 'Test Failed' )

export const run =
  title =>
  async ( test, ...tests ) =>
    {
      await test( title )

      if ( len( tests ) ) {
        await run( title )( ...tests )
      }
    }

export const test =
  ( message, fn, debug = false ) =>
  async title =>
    {
      try {
        await fn(  )

        console.log( green( `[ P ] - ${ title } - ${ message }` ) )
      } catch ( e ) {
        console.log( red( `[ F ] - ${ title } - ${ message }` ) )

        if ( debug ) {
          console.log( e )
        } else {
          console.log( red( e.message ) )
        }
      }
    }

export const assert = bool =>
  {
    if ( ! bool ) {
      throw Fail
    }
  }

export const assertEq =
  a =>
  b =>
    {
      if ( a !== b ) {
        throw Fail
      }
    }
