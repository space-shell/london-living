import { not } from './functions.mjs'
import { run, test, assertEq, assert } from './test.mjs'
import { html, trimLines } from './templates.mjs'

run
  ( 'Templates' )
  ( test
      ( 'Trims whitespace and removes lines'
      , async (  ) => {
          const string =
            `
            hello
            world
            `

          const string_trimmed = trimLines( string )

          const
            { length: l
            , [ 0 ]: first
            , [ l - 1 ]: last
            }
          = string_trimmed

          assert( not( string_trimmed.includes( '\n' ) ) )

          assert( first !== '' )

          assert( last !== '' )
        }
      )
  , test
      ( 'Removes newlines form html template'
      , async (  ) => {
          const x = 'world'

          const template =
            html`
              <div>
                <p>
                  Hello
                </p>

                <ul>
                  ${ 'good' }

                  <li>
                    ${ x }
                  </li>

                  ${ undefined }
                </ul>
              </div>
            `

          const
            { length: l
            , [ 0 ]: first
            , [ l - 1 ]: last
            }
          = template

          assert( not( template.includes( '\n' ) ) )

          assert( first !== '' )

          assert( last !== '' )
        }
      )
  )


