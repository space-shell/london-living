export const o =
  f =>
  g =>
  x =>
    f( g( x ) )

export const not =
  bool =>
   ! bool

export const match =
  regex =>
  string =>
    regex.test( string )

export const join =
  arr =>
    arr.join( '' )

export const len =
  list =>
    list.length

export const pipe =
  ( fn, ...fns ) =>
  x =>
    len( fns )
      ? pipe
          ( ...fns )
          ( fn( x ) )
      : fn( x )

export const pipeAsync =
  ( fn, ...fns ) =>
  async x =>
    len( fns )
      ? await pipeAsync
          ( ...fns )
          ( await fn( x ) )
      : await fn( x )

export const range =
  st =>
  fn =>
    Array.from
      ( { length: fn - st }
      , ( _, i ) => i + st
      )

export const tap =
  fn =>
  x =>
    ( fn( x )
    , x
    )

export const within =
  diff =>
  value =>
    ( value < diff ) && ( value > -diff )

export const tolerance =
  diff =>
  v1 =>
  v2 =>
    ( v2 - v1 < diff ) && ( v2 - v1 > -diff )

export const zipWith =
  f =>
  a =>
  b =>
    range( 0 )( Math.max( len( a ), len( b ) ) )
      .map( ( _, i ) => f( a[ i ], b[ i ] ) )

export const zip =
  zipWith( ( ...args ) => args )
