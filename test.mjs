import path from 'path'

import { getFiles, filePathDirPath } from './libs/files.mjs'
import { o, not, match } from './libs/functions.mjs'
import { red } from './libs/ansi.mjs'

const TEST_PATTERN = /.tests.mjs$/

const file_url = new URL( import.meta.url )

const file_dir = filePathDirPath( file_url )

;(async (  ) => {
  const files_application = await getFiles( path.resolve( file_dir, 'application' ) )

  const files_libs = await getFiles( path.resolve( file_dir, 'libs' ) )

  const tests = [ ...files_application, ...files_libs ]
    .filter( match( TEST_PATTERN ) )
    .map( async file => {
        try {
          await import( file )
        } catch ( e ) {
          console.log( red( e ) )
        }
      } )

  Promise.all( tests )
} )(  )
  
