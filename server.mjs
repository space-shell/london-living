import http from 'http'

import { routes } from './application/routes.mjs'

const HOST = '0.0.0.0'
const PORT = 8080

http
  .createServer( routes )
  .listen( PORT, HOST );

console.log(`Server running at http://${ HOST }:${ PORT }/`);
