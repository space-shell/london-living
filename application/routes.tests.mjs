import http from 'http'

import { not } from '../libs/functions.mjs'
import { run, test, assert } from '../libs/test.mjs'
import { routes, oops, handler } from './routes.mjs'

const STATUS_PASS =
  [ 200
  ]

const STATUS_FAIL =
  [ 404
  , 500
  ]

// Mock reponse callbacks
const mock_response =
  { writeHead: (  ) => {  }
  , end: (  ) => {  }
  }

// Creates a mock of the request callback arguments
const request_callback = ( url, method ) =>
  {
    const request = new http.IncomingMessage(  )

    request.url = url || request.url

    request.method = method || request.method

    return [ request , mock_response ]
  }

// Creates mock handlers
const get_home_handler = ([ status, [ request ] ]) =>
  handler
    ( 'GET' )
    ( '/', (  ) => [ true, null ] )
    ([ status, request ])

const post_home_handler = args =>
  handler
    ( 'POST' )
    ( '/', (  ) => [ true, null ] )
    ([ status, request ])

run
  ( 'Routes' )
  ( test
      ( 'Test to 404'
      , async (  ) => {
          const [ request, response ] = request_callback( '/google', 'GET' )

          const [ status, resp ] = await routes( request, response )

          assert( status === 404 )

          assert( /404 page not found/i.test( resp ) )
        }
      )
  , test
      ( 'Routes not 404'
      , async (  ) => {
          const [ request, response ] = request_callback( '/', 'GET' )


          const [ status, resp ] = await routes( request, response )

          assert( status !== 404 )

          assert( STATUS_PASS.includes( status ) )

          assert( not( /404 page not found/i.test( resp ) ) )
        }
      )
    , test
        ( 'Oops returns 404'
        , async (  ) => {
            const [ status, _ ] = oops([ null, null ])

            assert( status === 404 )
          }
        )
    , test
        ( 'Oops returns status'
        , async (  ) => {
            const [ status, _ ] = oops([ 200, null ])

            assert( status === 200 )
          }
        )
    , test
        ( 'GET Handler returns status'
        , async (  ) => {

            const [ status, _ ] =
              get_home_handler([ 200,  request_callback( '/', 'GET' )  ])

            assert( status === 200 )
          }
        )
    , test
        ( 'GET Handler returns controller'
        , async (  ) => {

            const [ status, _ ] =
              get_home_handler([ null,  request_callback( '/', 'GET' )  ])

            assert( status === true )
          }
        )
  )

