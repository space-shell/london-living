import { get, set, list } from './memcache.mjs'
import { geo_distance } from './geo-distance.mjs'
import { https_json } from '../../libs/https_json.mjs'
import { within } from '../../libs/functions.mjs'
import { sleep } from '../../libs/io.mjs'
import { not } from '../../libs/functions.mjs'

const API_URL = 'https://bpdts-test-app.herokuapp.com'

// Time before cached details are updates
const TTL = 600000
const SLEEP = 600000

const LONDON_LAT = 51.5
const LONDON_LNG = -0.116667

// 50 miles converted into kilometers
const DISTANCE = 80.4634695848

const from_london = geo_distance( LONDON_LAT, LONDON_LNG )

const within_catchment = ( lat, lng ) => 
  within( DISTANCE )( from_london( lat, lng ) )

/*
const spec_valid = url =>
  {
    - get remote spec details
    - compare spec details aginst stored details
    - return boolean
  }
*/

export const get_users_list = async (  ) =>
  https_json( `${ API_URL }/users` )

export const get_user = async id =>
  https_json( `${ API_URL }/user/${ id }` ) 

// Takes a list of users matching the schema format and dispatches async requests for additionsl information on each user
export const get_users = async users =>
  {
    const users_requests = users.map( ({ id }) => get_user( id ) )

    const users_responses = await Promise.allSettled( users_requests )

    return users_responses
      .filter( ({ status }) => status === 'fulfilled' )
      .map( ({ value }) => value )
  }

// Retrives the last TTL valur from memcache, ir not foune, sets the value and returns the newley set value
const retreive_ttl = async (  ) =>
  {
    const key = 'london_living_ttl'

    const stored_ttl = get( key )

    if ( stored_ttl ) {
      return stored_ttl
    }

    set( key, Date.now(  ) )

    return await retreive_ttl(  )
  }


// Retrieves data from the cache if availavle or updates the cache and retrieves the newley cached data
const retreive_users_list = async refresh =>
  {
    const key = 'london_living_users_list'

    if ( not( refresh ) ) {
      const users_list = get( key )

      if ( users_list !== false ) {
        return users_list
      }
    }

    set( key, await get_users_list(  ) ) 

    return await retreive_users_list( false )
  }

const retreive_users = async refresh =>
  {
    const key = 'london_living_users'

    if ( not( refresh ) ) {
      const users = get( key )

      if ( users !== false )
        return users
    }

    const users_list = await retreive_users_list( refresh )

    if ( users_list === null )
      return null

    set( key, await get_users( users_list ) ) 

    return await retreive_users( false )
  }

export const london_living = async refresh =>
  {
    const users = await retreive_users( refresh )

    if ( users === null )
      return

    const near_londoners = users
      .filter( ({ latitude, longitude }) => within_catchment( latitude, longitude ) )

    const londeners = users
      .filter( ({ city }) => Boolean( city ) )
      .filter( ({ city }) => /london/i.test( city ) )

    return [ londeners, near_londoners ]
      .flat( 2 )
      .map( ({ id }) => users.find( user => user.id === id ) )
      .sort( ( { id: a }, { id: b } ) => a - b )
  }

// Using the event loop the full user data can be updated periodically
// Worker threads would be prefered
/*
;(async function request_loop (  ) {
  await retreive_users( true )

  console.log( 'User Data Updated' )

  await sleep( SLEEP )

  return await request_loop(  )
} ) (  )
*/
