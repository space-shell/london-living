// Static values pre-calculated by external service
// https://www.geodatasource.com/distance-calculator

import { test, run, assert } from '../../libs/test.mjs'
import { tolerance } from '../../libs/functions.mjs'

import { geo_distance } from './geo-distance.mjs'

// The acceptable tolerance difference
const t = tolerance( 50 )

const distance_from = geo_distance( 34.003135, -117.7228641 )

run
  ( 'Geo Distance' )
  ( test
      ( 'Distance test 1'
      , async (  ) => {
          const d = distance_from( -2.9623869, 104.7399789 )

          assert( t( 14428.64 )( d ) )
        } 
      )
  , test
      ( 'Distance test 2'
      , async (  ) => {
          const d = distance_from( "15.45033", "44.12768" )

          assert( t( 14189.12 )( d ) )
        } 
      )
  , test
      ( 'Distance test 3'
      , async (  ) => {
          const d = distance_from( -26.94087, 29.24905 )

          assert( t( 16768.23 )( d ) )
        } 
      )
  , test
      ( 'Distance test 4'
      , async (  ) => {
          const d = distance_from( "-8.5139", "122.541" )

          assert( t( 13265.46 )( d ) )
        } 
      )
  )
    

