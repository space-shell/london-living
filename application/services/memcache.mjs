// Usage caveat - Normally in a service such as this IO actions that are not performed as expected would throw errors and delagate the handling responsibility to the client library, for the benefit of time IO acions return true or false. The down side to thes approach is tha stored boolean values may cause confusion upon implementation

import { not } from '../../libs/functions.mjs'

const cache =
  {
  }

export const get = key =>
  {
    if ( key in cache )
      return cache[ key ]
    else
      return false
  }

export const set = ( key, value ) =>
  {
    cache[ key ] = value

    return true
  }

export const list = (  ) => cache

