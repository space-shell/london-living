// https://en.wikipedia.org/wiki/Great-circle_distanceb
// https://www.math.ksu.edu/~dbski/writings/haversine.pdf

const { cos, sin, atan2, sqrt, PI } = Math

// Earths radius approx in KM
const R = 6356.752

const deg_rad = deg =>
  deg * ( PI / 180 )

export const geo_distance =
  ( lat_1, lng_1 ) =>
  ( lat_2, lng_2 ) =>
    {
      const delta_lat = deg_rad( lat_2 - lat_1 )

      const delta_lng = deg_rad( lng_2 - lng_1 )

      const x = sin( delta_lat / 2 ) ** 2

      const y = cos( deg_rad( lat_1 ) ) * cos( deg_rad( lat_2 ) )

      const z = sin( delta_lng / 2 ) ** 2

      const a = x + y * z

      return 2 * R * atan2( sqrt( a ), sqrt( 1 - a ) )
    }
