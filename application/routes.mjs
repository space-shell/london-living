import fs from 'fs'
import path from 'path'

import { home } from './controllers/home.mjs'
import { pipeAsync, tap, not } from '../libs/functions.mjs'

const mime_type = ext =>
  ( { 'js': 'text/javascript'
    , 'css': 'text/css'
    }[ ext ] || 'text/plain' )

// Handles request type, route and returns a tuple of the status code and response data
export const handler =
  method =>
  ( route, controller ) =>
  async ([ status, request, headers ]) =>
    {
      // All conditions must be met so that onle the applropriate controller is called
      if ( status !== null )
        return [ status, request, headers ]

      if ( request.method !== method )
        return [ status, request, headers ]

      if ( route !== request.url )
        return [ status, request, headers ]

      return await controller( request, headers )
    }

export const get = handler( 'GET' )

export const broken = ([ status, request, headers ]) =>
  {
    if ( status === 500 )
      return [ 500, 'Something went wrong', headers ]

    return[ status, request, headers ]
  }

export const oops = ([ status, request, headers ]) =>
  {
    if ( status !== null )
      return [ status, request, headers ]

    return[ 404, 'Not Found', headers ]
  }

// Takes a list of paths to accept and returns any matching files if found
export const files =
  root =>
  match =>
  async ([ status, request, headers ]) =>
  {
    if ( status !== null )
      return [ status, request, headers ]

    if ( not( match.test( request.url ) ) )
      return [ status, request, headers ]
    
    try {
      const file_path = path.join
        ( root
        , request.url
        )

      const file = await
        fs.promises.readFile( file_path )

      const mime = mime_type( request.url.split('.').pop() );

      return [ 200, file, { ...headers, 'Content-Type': mime } ]
    } catch ( e ) {
      console.warn( e )

      return [ status, request, headers ]
    }
  }

const public_files = files( './public' )

export const routes = async ( request, response ) => {

  // Pipes the request throught all of the async in series
  const [ status, resp, headers ] = await pipeAsync
    ( get( '/', home(  ) )
    , public_files( /^\/js/ )
    , public_files( /^\/css/ )
    , public_files( /^\/favicon\.ico/ )
    , broken
    , oops
    )
    ( [ null, request, {  } ] )

  response.writeHead( status, headers )

  response.end( resp, 'utf-8' );

  return [ status, resp ]
}
