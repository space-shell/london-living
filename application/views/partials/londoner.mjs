import { html } from '../../../libs/templates.mjs'

const londoner_row = ({ id, first_name, last_name, email }) =>
  html`
    <tr>
      <td class='londoner__cell--id'>
        ${ id }
      </td>
      <td class='londoner__cell--first_name'>
        ${ first_name }
      </td>
      <td class='londoner__cell--last_name'>
        ${ last_name }
      </td>
      <td class='londoner__cell--email'>
        <a href=${ email }>
          ${ email }
        </a>
      </td>
    </tr> `

export const londoner =
  londoners =>
  content =>
    html`
      <table class='londoner__table'>
        <thead>
          <tr class='londoner__head'>
            <td class='londoner__cell--id'>
              ID
            </td>
            <td class='londoner__cell--first_name'>
              First Name
            </td>
            <td class='londoner__cell--last_name'>
              Last Name
            </td>
            <td class='londoner__cell--email'>
              Email
            </td>
          </tr>
        </thead>
        <tbody>
          ${ londoners
              .map( londoner_row )
              .join( '' )
          }
        </tbody>
      </table> `
