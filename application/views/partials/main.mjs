import { html } from '../../../libs/templates.mjs'

import { page } from './page.mjs'
import { header, scripts, styles } from './header.mjs'
import { footer } from './footer.mjs'

const CDN_JS =
  [
  ]

const CDN_CSS =
  [ '/css/styles.css'
  ]

export const main =
  ( { title
    , js
    , css
    , ...state
    } ) =>
  content =>
    page
      ( { header: header
            ({ ...state, title })
            ( html`
                ${ scripts([ ...CDN_JS, ...( js || [  ] ) ]) }
                ${ styles([ ...CDN_CSS, ...( css || [  ] ) ]) } ` )
        , footer: footer
            ({ ...state })
            (  )
        } )
      ( content )
