import { html } from '../../../libs/templates.mjs'

export const styles = scr =>
  scr.map( script => html`
        <link href="${ script }" rel="stylesheet">
        </link> ` )
      .join( '' )

export const scripts = scr =>
  scr.map( script => html`
        <script src="${ script }">
        </script> ` )
      .join( '' )

export const header =
  ({ title, scripts }) =>
  content =>
    html`
      <title>
        ${ title }
      </title>

      <meta name="viewport" content="width=device-width,initial-scale=1.0"></meta>

      ${ content } `
