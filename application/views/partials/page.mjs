import { html } from '../../../libs/templates.mjs'

export const page =
  ({ header, footer }) =>
  content =>
    html`
      <!DOCTYPE html>

      <html lang='en' class='page'>
        <head class='page__title'>
          ${ header }
        </head>

        <body class='page__body'>
          <main class='page__main'>
            ${ content }
          </main>

          <footer class='page__footer'>
            ${ footer }
          </footer>
        </body>
      </html> `
