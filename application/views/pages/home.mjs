import { html } from '../../../libs/templates.mjs'
import { main } from '../partials/main.mjs'
import { londoner } from '../partials/londoner.mjs'

export const home = ({ londoners, ...state }) =>
  main
    ( { ...state
      , title: 'London Living | Home'
      , css:
          [ '/css/pages/home.css'
          ]
      }
    )
    ( html`
        <header class='home__header'>
          <h1 class='home_title'>
            London Living
          </h1>
        </header>

        <p class='home__info'>
          Currently displaying a table of users who are either registed as living in London or who's location information show that are living within 50 miles of London.
        </p>

        <section class='home_data'>
          ${ londoner( londoners )(  ) }
        </section> ` )

