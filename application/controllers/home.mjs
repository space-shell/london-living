import { london_living } from '../services/london-living.mjs'

import { home as template } from '../views/pages/home.mjs'

export const home =
  middleware =>
  async ( req, headers ) =>
    {
      const londoners = await london_living( false )
      
      if ( londoners === null ) {
        return [ 500, req ]
      }

      return [ 200, await template({ londoners }), { ...headers, 'Content-Type': 'text/html' } ]
    }
