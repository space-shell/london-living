# London Living

This project provides a service that displays user information as a web page based on whether or not the user lives near the London ( UK ) area.

## Requirements

Any version of Node that supports the ES6 module system.

**Feature Flagged**
- Node v12+

**Experimental**
- Node v13+

> [ Get Node ]( https://nodejs.org/en/download )


## Project Structure

```bash
├── application
│   ├── controllers - Request handlers
│   ├── services - Shareable logic
│   ├── middleware - Response transformers
│   └── views
│       ├── pages - Route templates
│       └── partials - HTML template fragments
├── libs - Utility functions
```

## Project Architecture

The project is founded on an ~~M~~VC pattern using a functional programming discpline.

The functional approach provides a less stateful architecture.

A overview of the system is that the `views` submit requests that are processed by the `controllers` that in turn produce `views`. The service layer at the moment provides data storage and manipulation capabilities but has the potential of expansion.

## Lighthouse Assessment

According to the Lighthouse auditor that comes with the Google Chrome browser, the web page achieves the following results.

|Performance|Accessibility|Best Practices|SEO|
|:-:|:-:|:-:|:-:|
|100|100|93|90|

> This result is achieved once the cache has been populated.


## Template Engine

The `views` are produced through the use of ES6 template strings. The state is piped from the controller through each page / partial template and a resulting html string is produced.

The `html` function from the `libs/templates.mjs` file simply strips newlines and trims whitespace from the template strings to avoid whitespace conflicting with any CSS styling.


## Unit Testing

> Unit test files should be located alongside any associated files and adhere to the following format `*.tests.mjs`

The unit testing runs the tests in series and awaits any asynchronous callbacks. Any test that doesn't throw an exception is considered a pass. The `assert` helper function takes a boolean value and throws a `Fail` exception upon falsey values.


## Rapid development

**Requirements**
- entr


```bash
# To install entr
sudo apt install entr
```

For a more rapid development environment the command `entr` can be used to both run the test suite and reload the server upon detecting file changes.

**Example**

```bash
# To start the server
ls **/*.mjs | entr -rc node server.mjs

# To start the test suite
ls **/*.mjs | entr -rc node test.mjs
```

## Security Considerations

Having no user interaction with the backend drastically reduces the potential of cross-site scripting attacks. Having no SQL integration also prevents a number of persistant storage attacks.

To prevent DDOS attacks a simple rate limiter could be put in place at the initial point of request and caching data collected from any external services reduces the risk of DDOS propogation.


## Performance Considerations

Having the site rendered on the server means that the user is provided with all of the requested data at once. The lack of any AJAX requests ensures a more consistant page loading time and reducing the client side JavaScript complexity to 0 decreases the CPU processing required.

Statically displayed content over dynamically loaded content using JavaScript provides a number of benefits.

- Accessibility, screen readers function better with static content
- SEO optimal


## Accessibility Considerations

The page is styled responsively, using semantic HTML and uses high contrast colours to provide better visual accessibility.   


## Accessibility Considerations

The omission of client side JavaScript increases the compatibility rate accross all devices.


## Background process

The `london-living` module provides functionality to retrieve user data, in order to collect the full set of user data a large number of https requests must be dispatched. In order to avoid blocking the call stack these requests are dispatched to the event loop to be processed in the background.

The following code extract shows how the retrieval of user data is constantly dispatched to the event loop in order to keep the cache updated.

```javascript
;(async function request_loop (  ) {
  await retreive_users( true )

  await sleep( SLEEP )

  return await request_loop(  )
} ) (  )
```
